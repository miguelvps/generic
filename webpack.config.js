const path = require('path')

module.exports = {
  entry: './src/web.tsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'static'),
    publicPath: '/static/',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
          loader: 'ts-loader',
          options: {
            compilerOptions: { 'jsx': 'react' },
            transpileOnly: true,
          },
        },
      },
    ],
  },
  resolve: {
    extensions: [ '.js', '.jsx', '.ts', '.tsx' ]
  },
  devServer: {
    publicPath: '/static/',
    proxy: { '*': 'http://localhost:5000' }
  },
};
