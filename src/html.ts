export default (root: string, state: any) => `
<!doctype html>
<html>
  <head>
    <title>Generic</title>
    <link rel="icon" href="/static/favicon.ico">
  </head>
  <body>
    <div id="root">${root}</div>
    <script>window.__PRELOADED_STATE__ = ${JSON.stringify(state)}</script>
    <script src="/static/bundle.js"></script>
  </body>
</html>
`
