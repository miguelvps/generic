import * as express from "express"
import * as React from "react"
import { renderToString } from "react-dom/server"
import { Provider } from "react-redux"
import { StaticRouter } from "react-router"
import { createStore } from "redux"
import App from "./components/App"
import html from "./html"
import reducers from "./reducers"

function ssr(req: express.Request, res: express.Response) {
  const store = createStore(reducers)
  const context = {}
  const root = renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <App/>
      </StaticRouter>
    </Provider>,
  )

  res.send(html(root, {}))
}

export default ssr
