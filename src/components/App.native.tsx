import * as React from "react"
import { StyleSheet, Text, View } from "react-native"

export default class App extends React.Component<{}> {
  public render() {
    return (
      <View style={styles.container}>
        <Text>This is a native component!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    justifyContent: "center",
  },
})
