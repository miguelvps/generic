import { createBrowserHistory } from "history";
import * as React from "react"
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux"
import { ConnectedRouter, routerMiddleware, routerReducer } from "react-router-redux"
import { applyMiddleware, combineReducers, createStore } from "redux"
import App from "./components/App"
import reducers from "./reducers"

function createRootElement(Component: React.StatelessComponent) {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Component />
      </ConnectedRouter>
    </Provider>
  )
}

function createRootReducer(reducers1: any) {
  return combineReducers({
    ...reducers1,
    routing: routerReducer,
  })
}

const history = createBrowserHistory()
const middleware = routerMiddleware(history)
const store = createStore(createRootReducer(reducers), applyMiddleware(middleware))

ReactDOM.hydrate(createRootElement(App), document.getElementById("root"))

if (module.hot) {
  module.hot.accept("./components/App", () => {
    const NewApp = require("./components/App").default
    ReactDOM.render(createRootElement(NewApp), document.getElementById("root"))
  })

  module.hot.accept("./reducers", () => {
    const newReducers = require("./reducers").default;
    store.replaceReducer(createRootReducer(newReducers))
  });
}
