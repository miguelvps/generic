import * as express from "express"
import * as graphqlHTTP from "express-graphql"
import * as fs from "fs"
import { makeExecutableSchema } from "graphql-tools"
import * as path from "path"
import "reflect-metadata";
import {createConnection, DefaultNamingStrategy, getConnectionOptions, NamingStrategyInterface} from "typeorm"
import {DatabaseNamingStrategy} from "./models"
import Context from "./models/Context"
import Session from "./models/Session"
import resolvers from "./resolvers"
import ssr from "./ssr"

getConnectionOptions().then((connectionOptions) => {
  return createConnection(Object.assign(connectionOptions, {
    namingStrategy: new DatabaseNamingStrategy(),
  }))
});

const app = express()

app.use("/static", express.static("static"))

app.use("/graphql", async (req: express.Request, res: express.Response, next: express.NextFunction) => {
  // FIXME
  const token = req.headers && req.headers.authorization && req.headers.authorization.split(" ")[2]
    || req.query && req.query.access_token
  const session = await Session.load(token)
  const context = new Context(req, res, session)

  return graphqlHTTP({
    context,
    graphiql: true,
    schema: makeExecutableSchema({
      resolvers,
      typeDefs: fs.readFileSync(path.join(__dirname, "../src/schema.gql"), "utf8"),
    }),
  })(req, res)
})

app.use(ssr)

app.listen(5000, () => {
  console.info("Listening...")
})
