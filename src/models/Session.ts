import * as crypto from "crypto"
import redis from "../redis"
import User from "./User"

export default class Session {
  public static async load(id?: string): Promise<Session> {
    if (!id) {
      return new Session()
    }
    const data = await redis.hgetall(`session:${id}`)
    const user = data.user_id && await User.repository.findOne({id: data.user_id})
    return new Session(id, user)
  }

  public id: string
  public user?: User

  constructor(id?: string, user?: User) {
    this.id = id || crypto.randomBytes(16).toString("hex")
    this.user = user
  }

  public async save() {
    await redis.hmset(`session:${this.id}`, {
      user_id: this.user ? this.user.id : null,
    })

    return this
  }
}
