import {
  Column,
  CreateDateColumn,
  Entity,
  EntityManager,
  getRepository,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import User from "./User"

@Entity()
export default class Group {
  public static getRepository(manager?: EntityManager) {
    return manager ? manager.getRepository(Group) : getRepository(Group)
  }

  static get repository() {
    return Group.getRepository()
  }

  @PrimaryGeneratedColumn("uuid")
  public id: string

  @Column({ length: 255 })
  public name: string

  @CreateDateColumn({ type: "timestamptz" })
  public createdAt: Date

  @UpdateDateColumn({ type: "timestamptz" })
  public updatedAt: Date

  @ManyToMany((type) => User, (user) => user.groups)
  public users: User[];
}
