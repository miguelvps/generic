import {Request, Response} from "express"
import Session from "./Session"

export default class Context {
  public session: Session
  public req: Request
  public res: Response

  constructor(req: Request, res: Response, session: Session) {
    this.req = req
    this.res = res
    this.session = session
  }
}
