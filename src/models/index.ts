import {snakeCase} from "lodash"
import {DefaultNamingStrategy, getConnectionOptions, NamingStrategyInterface, Table} from "typeorm"

export class DatabaseNamingStrategy extends DefaultNamingStrategy implements NamingStrategyInterface {
  public tableName(targetName: string, userSpecifiedName: string): string {
    return userSpecifiedName ? userSpecifiedName : snakeCase(targetName)
  }

  public columnName(propertyName: string, customName: string, embeddedPrefixes: string[]): string {
    return snakeCase(embeddedPrefixes.concat(customName ? customName : propertyName).join("_"))
  }

  public columnNameCustomized(customName: string): string {
    return customName;
  }

  public relationName(propertyName: string): string {
    return snakeCase(propertyName);
  }

  public joinTableName(firstTableName: string, secondTableName: string): string {
    return firstTableName + "_" + secondTableName
  }

  public primaryKeyName(tableOrName: Table|string, columnNames: string[]): string {
    const tableName = tableOrName instanceof Table ? tableOrName.name : tableOrName
    return `${tableName}_pkey`
  }

  public uniqueConstraintName(tableOrName: Table|string, columnNames: string[]): string {
    const sortedColumnNames = [...columnNames].sort()
    const tableName = tableOrName instanceof Table ? tableOrName.name : tableOrName
    return `${tableName}_${sortedColumnNames.join("_")}_unique`
  }

  public foreignKeyName(tableOrName: Table|string, columnNames: string[]): string {
    const sortedColumnNames = [...columnNames].sort()
    const tableName = tableOrName instanceof Table ? tableOrName.name : tableOrName;
    return `${tableName}_${sortedColumnNames.join("_")}_foreign`
  }

  public indexName(tableOrName: Table|string, columnNames: string[], where?: string): string {
    const sortedColumnNames = [...columnNames].sort()
    const tableName = tableOrName instanceof Table ? tableOrName.name : tableOrName;
    const suffix = where ? `_${where}` : ""

    return `${tableName}_${sortedColumnNames.join("_")}${suffix}_idx`
  }
}
