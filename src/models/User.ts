import * as bcrypt from "bcrypt"
import {
  Column,
  CreateDateColumn,
  Entity,
  EntityManager,
  getRepository,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import Group from "./Group"

@Entity()
export default class User {
  public static getRepository(manager?: EntityManager) {
    return manager ? manager.getRepository(User) : getRepository(User)
  }

  static get repository() {
    return User.getRepository()
  }

  @PrimaryGeneratedColumn("uuid")
  public id: string

  @Column({ length: 255, unique: true })
  public email: string

  @Column({ length: 255 })
  public passwordHash: string

  @CreateDateColumn({ type: "timestamptz" })
  public createdAt: Date

  @UpdateDateColumn({ type: "timestamptz" })
  public updatedAt: Date

  @ManyToMany((type) => Group, (group) => group.users, { cascade: true })
  @JoinTable()
  public groups: Group[];

  public async setPassword(password: string) {
    this.passwordHash = await bcrypt.hash(password, 10)
  }

  public async verifyPassword(password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.passwordHash)
  }
}
