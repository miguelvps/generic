import {omit} from "lodash"
import Context from "../models/Context"
import Session from "../models/Session"
import User from "../models/User"

export const Query = {
  users: async () => {
    return await User.repository.find()
  },

  viewer: (root: any, args: any, context: Context): User|undefined => {
    return context.session.user
  },

  viewerSession: (root: any, args: any, context: Context): Session => {
    return context.session
  },
}

export const Mutation = {
  createUser: async (root: any, { input }: any): Promise<User> => {
    const user = new User()
    Object.assign(user, omit(input, "password"))
    await user.setPassword(input.password)
    return await User.repository.save(user)
      .catch((err) => {
        if (err.constraint === "user_email_unique") {
          throw new Error("Email already exists")
        }
        throw err
      })
  },

  updateUser: async (root: any, { input }: any): Promise<User> => {
    const user = await User.repository.findOne({ id: input.id })
    if (!user) { throw new Error("User not found") }
    Object.assign(user, omit(input, "password"))
    if (input.password) {
      await user.setPassword(input.password)
    }
    return await User.repository.save(user)
  },

  login: async (root: any, { email, password }: any, context: Context): Promise<Session> => {
    const user = await User.repository.findOne({ email })
    if (!user || !user.verifyPassword(password)) {
      throw new Error("Incorrect username or password")
    }

    context.session.user = user
    return context.session.save()
  },
}

const UserResolver = {
  id(user: User) {
    return user.id
  },

  email(user: User) {
    return user.email
  },
}
export {UserResolver as User}
