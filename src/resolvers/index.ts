import { merge } from "lodash"
import * as user from "./user"

export default merge({}, user)
